package br.com.soluti.julianocampos.bean;

import java.io.IOException;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import br.com.soluti.julianocampos.model.Certificado;
import br.com.soluti.julianocampos.services.CertificadoService;

@Model
public class CertificadoDetalhesBean {
	private Certificado certificado = new Certificado();
	private Integer  certificadoId;
	@Inject
	private CertificadoService certificadoService;
	

	public Integer getCertificadoId() {
		return certificadoId;
	}

	public void setCertificadoId(Integer certificadoId) {
		this.certificadoId = certificadoId;
		this.certificado = certificadoService.certificado(certificadoId);
	}

	public Certificado getCertificado() {
		return certificado;
	}
	
}
