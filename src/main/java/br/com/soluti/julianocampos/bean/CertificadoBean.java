package br.com.soluti.julianocampos.bean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import br.com.soluti.julianocampos.model.Certificado;
import br.com.soluti.julianocampos.services.CertificadoService;

@Model
public class CertificadoBean {
	private List<Certificado> certificados = new ArrayList<>();
	
	@Inject
	private CertificadoService certificadoService;
	
	public List<Certificado> getCertificados() {
		this.certificados = certificadoService.listar();
		return this.certificados;
	}
	
	public String detalhes(Certificado certificado) {
		return "/detalhes?faces-redirect=true&id="+certificado.getId();
	}
	
	public String excluir(Certificado certificado) {
		certificadoService.excluir(certificado);
		return "/index?faces-redirect=true";
	}

}
