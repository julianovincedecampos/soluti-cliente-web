package br.com.soluti.julianocampos.bean;

import java.io.IOException;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.servlet.http.Part;

import br.com.soluti.julianocampos.model.Certificado;
import br.com.soluti.julianocampos.services.CertificadoService;

@Model
public class CertificadoFormBean {
	private Certificado certificado = new Certificado();
	private Part certificadoPath;
	
	@Inject
	private CertificadoService certificadoService;

	public Certificado getCertificado() {
		return certificado;
	}

	public void setCertificado(Certificado certificado) {
		this.certificado = certificado;
	}

	public String enviar() throws IOException {
		certificadoService.cadastrar(certificado, certificadoPath);
		return "/index?faces-redirect=true";
	}

	public Part getCertificadoPath() {
		return certificadoPath;
	}

	public void setCertificadoPath(Part certificadoPath) {
		this.certificadoPath = certificadoPath;
	}

}
