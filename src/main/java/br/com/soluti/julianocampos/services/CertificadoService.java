package br.com.soluti.julianocampos.services;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Part;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.soluti.julianocampos.infra.FileSaver;
import br.com.soluti.julianocampos.model.Certificado;
import br.com.soluti.julianocampos.model.CertificadoDetalhes;

public class CertificadoService {
	public List<Certificado> listar() {
		Client client = ClientBuilder.newClient();
		String target = "http://localhost:8080/soluti-ws/services/certificado/listar";
		WebTarget webTarget = client.target(target);
		Builder request = webTarget.request();

		Response response = request.get();
		Gson gson = new Gson();

		String json = response.readEntity(String.class);

		TypeToken<List<Certificado>> token = new TypeToken<List<Certificado>>() {
		};
		List<Certificado> certificados = gson.fromJson(json, token.getType());

		return certificados;
	}
	
	public Certificado certificado(Integer certificadoId) {
		Gson gson = new Gson();
		String client = ClientBuilder.newClient()
				.target("http://localhost:8080/soluti-ws/services/certificado/")
				.path("/detalhes/?").queryParam("certificadoId", certificadoId)
				.request("application/json")
				.get(String.class);
		 Certificado certificadoDetalhes =gson.fromJson(client, Certificado.class); 
		return certificadoDetalhes;
	}

	public void cadastrar(Certificado certificado, Part certificadoPath) throws IOException {
		FileSaver fileSaver = new FileSaver();
		fileSaver.write(certificado,certificadoPath);
		
		Client client = ClientBuilder.newClient();
        String target = "http://localhost:8080/soluti-ws/services/certificado/gravar";
        Entity<Certificado> json = Entity.json(certificado);
        System.out.println(json.toString());
        String post = client.target(target).request().post(json, String.class);
		System.out.println(post);
		System.out.println(certificado.getCertificado());
	}

	public String excluir(Certificado certificado) {
		String client = ClientBuilder.newClient()
				.target("http://localhost:8080/soluti-ws/services/certificado/")
				.path("/delete/?").queryParam("certificadoId", certificado.getId())
				.request("application/json")
				.delete(String.class);
		System.out.println(client);
		return client;
	}

}
