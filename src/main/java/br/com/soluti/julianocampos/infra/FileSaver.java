package br.com.soluti.julianocampos.infra;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.Part;

import br.com.soluti.julianocampos.model.Certificado;

public class FileSaver {
	public static final String TEMP = "C://arquivos_temp/pem/";

	public void write(Certificado certificado, Part arquivo) {
		String relativePath = TEMP + "/" + arquivo.getSubmittedFileName();
		try {
			arquivo.write(relativePath);
		} catch (IOException e) {
			System.out.println("Arquivo já existe no diretorio.");
		}
		gravaCertificado(relativePath, certificado);
	}

	public void gravaCertificado(String arquivo, Certificado certificado) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(arquivo));
			String linha = "";
			while (br.ready()) {
				linha += br.readLine();
				System.out.println(linha);
			}
			br.close();
			certificado.setCertificado(deconding(arquivo));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public String deconding(String arquivo) {
		String certificado = "";
		try {
			FileInputStream fis = new FileInputStream(arquivo);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			Collection c = cf.generateCertificates(fis);
			Iterator i = c.iterator();

			while (i.hasNext()) {
				Certificate cert = (Certificate) i.next();
				certificado = cert.toString();
			}
			System.out.println(certificado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return certificado;
	}

}
